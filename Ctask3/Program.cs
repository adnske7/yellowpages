﻿using System;

namespace Ctask3
{
	class Program
	{
		static void Main(string[]args)
		{
			string[] firstNames = { "Jim", "Jhon", "Fred", "Joe", "James" };
			string[] lastNames = { "Jimson", "Jhonson", "Fredson", "Joeson", "Jameson" };
			Console.WriteLine("Search for a name...");
			string userInput = Console.ReadLine();
			for (int i = 0; i < firstNames.Length; i++) {
				string tempName = firstNames[i] + " " + lastNames[i];
				if (tempName.ToLower().Contains(userInput.ToLower()))
				{
					Console.WriteLine("Match found! Name:" + tempName);
				}
			}

			
		}
	}
}
