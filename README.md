# YellowPages
A short program that allows the user to search for matches in an array of people, and returns the full name of everyone that matches the search.

## Getting Started
Open the folder in Visual studio, and run the program.cs file.

## Author

* **Ådne Skeie** - [adnske7](https://gitlab.com/adnske7)